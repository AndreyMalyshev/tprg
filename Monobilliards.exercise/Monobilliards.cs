﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
using System;
using System.Collections.Generic;

namespace Monobilliards
{
	public class Monobilliards : IMonobilliards
	{
		public bool IsCheater(IList<int> inspectedBalls)
		{
            var rez = false;

            var kit = new Queue<int>();

            foreach (var ball in inspectedBalls)
            {
                kit.Enqueue (ball);
            }
            if (kit.Count > 0)
            {

            int currentBall = kit.Dequeue();
            while (kit.Count>0)
            {
                int tmpBall = kit.Dequeue();
                if (Math.Abs(tmpBall-currentBall)!=1)
                {
                    rez = true;
                    break;
                }
                currentBall = tmpBall;
           }
            }
            return rez;
		}
	}
}

/*
Error on: Hidden test #5 
Output 0th element should be Not a proof, but was Cheater
Error on NUnit test: RunAllTests checking.AssertionException : Output 0th element should be Not a proof, but was Cheater    at checking.ComparisonExtensions.ShouldBe(Object actual, Object expected, String description, Object[] args)
   at checking.ComparisonExtensions.ShouldBe[T](ICollection`1 actual, ICollection`1 expected, String description)
   at checking.AcmLikeTestCase.Run()
   at checking.CheckerRunner.RunAllTests()
 */
