<Query Kind="Program" />

void Main()
{
	Queue<int> q=new Queue<int>();
	for (int i=1;i<4;i++){
	q.Enqueue(i+10);
	}
	
	for(int i=1;i<5;i++){
		q.Dequeue().Dump();
	}
}

// Define other methods and classes here
public class QueueItem{
	public QueueItem(int value) {
		Value=value;
		Next=null;
	}
	public int Value{get;set;}
	public QueueItem Next{get;set;}
}

public class Queue{
	QueueItem head=null;
	QueueItem tail=null;
	
	public void Enqueue(int value){
		QueueItem item=new QueueItem(value);
		if(head==null) 
		{
		tail=head=item;
		}
		else{
			tail.Next=item;
			tail=item;
		}		
	}
	
	public int Dequeue(){
		if(head==null) throw new InvalidOperationException("Queue is empty");
		int rez=head.Value;
		head=head.Next;
		if (head==null) tail=null;
		return rez;
	}
}

public class QueueItem<T>{
	public QueueItem(T value) {
		Value=value;
		Next=null;
	}
	public T Value{get;set;}
	public QueueItem<T> Next{get;set;}
}

public class Queue<T>{
	QueueItem<T> head;
	QueueItem<T> tail;
	
	public void Enqueue(T value){
		QueueItem<T> item=new QueueItem<T>(value);
		if(head==null)
			tail=head=item;
			
		else{
			tail.Next=item;
			tail=item;
		}
	}
	
	public T Dequeue(){
		T rez=head.Value;
		head=head.Next;
		if (head==null){tail=null;}
		return rez;
	}
}