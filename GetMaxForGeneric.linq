<Query Kind="Program" />

void Main()
{
	Console.WriteLine(Max(new int[0]));
    Console.WriteLine(Max(new[] { 3 }));
    Console.WriteLine(Max(new[] { 5, 1, 2 }));
    Console.WriteLine(Max(new[] { "A", "B", "C" }));

}

// Define other methods and classes here
static TValue Max<TValue>(TValue[] source) where TValue:IComparable
{
	TValue rez=default(TValue);
    if(source.Length == 0)
        return rez;
    foreach(TValue e in source){
		 if (e.CompareTo(rez) > 0) rez = e;
	}
	return rez;
}
      